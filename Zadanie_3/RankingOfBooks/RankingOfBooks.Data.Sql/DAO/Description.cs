﻿namespace RankingOfBooks.Data.Sql.DAO
{
    public class Description
    {
        public Description(){}
        
        public int DescriptionId { get; set; }
        public string Contents { get; set; }
        
        public Book Book { get; set; }
    }
}
﻿namespace RankingOfBooks.Data.Sql.DAO
{
    public class BookRating
    {
        public BookRating(){}
        
        public int BookRatingId { get; set; }
        public int Value { get; set; }
        
        public Book Book { get; set; }
    }
}
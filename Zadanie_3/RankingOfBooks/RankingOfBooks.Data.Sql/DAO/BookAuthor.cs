﻿namespace RankingOfBooks.Data.Sql.DAO
{
    public class BookAuthor
    {
        public BookAuthor(){}
        
        public int BookAuthorId { get; set; }
        
        public int BookId { get; set; }
        public Book Book { get; set; }
        
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
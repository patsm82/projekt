﻿using System.Collections;
using System.Collections.Generic;

namespace RankingOfBooks.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Books = new List<Book>();
            Comments = new List<Comment>();
        }
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsActiveUser { get; set; }
        public bool IsBannedUser { get; set; }
        
        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        
    }
}
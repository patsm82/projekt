﻿namespace RankingOfBooks.Data.Sql.DAO
{
    public class Photo
    {
        public Photo(){}
        
        public int PhotoId { get; set; }
        public string ImageHref { get; set; }
        
        public Book Book { get; set; }
    }
}
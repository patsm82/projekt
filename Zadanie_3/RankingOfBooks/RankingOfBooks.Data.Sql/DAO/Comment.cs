﻿using System;

namespace RankingOfBooks.Data.Sql.DAO
{
    public class Comment
    {
        public Comment()
        {
        }

        public int CommentId { get; set; }
        public DateTime Date { get; set; }
        public string Contents { get; set; }

        public User User { get; set; }
#nullable enable
        public Comment? AnotherComment { get; set; }
#nullable disable
        public Book Book { get; set; }
    }
}
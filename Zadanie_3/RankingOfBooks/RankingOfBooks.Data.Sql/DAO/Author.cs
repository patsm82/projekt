﻿using System.Collections;
using System.Collections.Generic;

namespace RankingOfBooks.Data.Sql.DAO
{
    public class Author
    {

        public Author()
        {
            BookAuthors = new List<BookAuthor>();
        }
        
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string AuthorSurname { get; set; }
        
        public virtual ICollection<BookAuthor> BookAuthors { get; set; } 
    }
}
﻿using System.Collections;
using System.Collections.Generic;

namespace RankingOfBooks.Data.Sql.DAO
{
    public class Category
    {

        public Category()
        {
            BookCategories = new List<BookCategory>();
        }
        
        public int CategoryId { get; set; }
        public string Name { get; set; }
        
        public virtual ICollection<BookCategory> BookCategories { get; set; } 
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;

namespace RankingOfBooks.Data.Sql.DAO
{
    public class Book
    {
        public Book()
        {
            BookCategories = new List<BookCategory>();
            Photos = new List<Photo>();
            BookRatings = new List<BookRating>();
            Descriptions = new List<Description>();
            Comments = new List<Comment>();
            BookAuthors = new List<BookAuthor>();
        }
        
        public int BookId { get; set; }
        public string Title { get; set; }
        public int Pages { get; set; }
        
        #nullable enable
        public User? User { get; set; }
        #nullable disable
        
        public virtual ICollection<BookCategory> BookCategories { get; set; } 
        public virtual ICollection<Photo> Photos { get; set; }
        public virtual ICollection<BookRating> BookRatings { get; set; }
        public virtual ICollection<Description> Descriptions { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<BookAuthor> BookAuthors { get; set; }
    }
}
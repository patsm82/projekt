﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.UserName).IsRequired();
            builder.Property(u => u.Email).IsRequired();
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.Property(a => a.AuthorName).IsRequired();
            builder.Property(a => a.AuthorSurname).IsRequired();
        }
    }
}
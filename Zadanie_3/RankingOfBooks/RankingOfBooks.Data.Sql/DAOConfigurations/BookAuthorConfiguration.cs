﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class BookAuthorConfiguration : IEntityTypeConfiguration<BookAuthor>
    {
        public void Configure(EntityTypeBuilder<BookAuthor> builder)
        {
            builder.HasOne(bc => bc.Book)
                .WithMany(bc => bc.BookAuthors)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(bc => bc.BookId);
            
            builder.HasOne(bc => bc.Author)
                .WithMany(bc => bc.BookAuthors)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(bc => bc.AuthorId);
        }
    }
}
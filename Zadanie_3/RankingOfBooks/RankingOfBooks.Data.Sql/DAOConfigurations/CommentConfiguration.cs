﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.Property(c => c.Contents).IsRequired();
            builder.Property(c => c.Date).IsRequired();
        }
    }
}
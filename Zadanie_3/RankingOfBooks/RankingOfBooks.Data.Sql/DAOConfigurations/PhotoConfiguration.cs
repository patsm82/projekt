﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class PhotoConfiguration : IEntityTypeConfiguration<Photo>
    {
        public void Configure(EntityTypeBuilder<Photo> builder)
        {
            builder.Property(p => p.ImageHref).IsRequired();
        }
    }
}
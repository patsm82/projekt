﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.DAOConfigurations
{
    public class BookCategoryConfiguration : IEntityTypeConfiguration<BookCategory>
    {
        public void Configure(EntityTypeBuilder<BookCategory> builder)
        {
            builder.HasOne(bc => bc.Book)
                .WithMany(bc => bc.BookCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(bc => bc.BookId);
            
            builder.HasOne(bc => bc.Category)
                .WithMany(bc => bc.BookCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(bc => bc.CategoryId);
        }
    }
}
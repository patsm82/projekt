﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using RankingOfBooks.Data.Sql.DAO;

namespace RankingOfBooks.Data.Sql.migrations
{
    public class DatabaseSeed
    {
        private readonly RankingOfBooksDbContext _context;

        public DatabaseSeed(RankingOfBooksDbContext context)
        {
            this._context = context;
        }

        public void Seed()
        {
            #region CREATE AUTHORS

            var authors = BuildAuthors();
            _context.Authors.AddRange(authors);
            _context.SaveChanges();

            #endregion

            #region CREATE USERS

            var users = BuildUsers();
            _context.Users.AddRange(users);
            _context.SaveChanges();

            #endregion

            #region CREATE CATEGORIES

            var categories = BuildCategories();
            _context.Categories.AddRange(categories);
            _context.SaveChanges();

            #endregion

            #region CREATE BOOKS

            var books = BuildBooks(users);
            _context.Books.AddRange(books);
            _context.SaveChanges();

            #endregion

            #region CREATE PHOTOS

            var photos = BuildPhotos(books);
            _context.Photos.AddRange(photos);
            _context.SaveChanges();

            #endregion

            #region CREATE COMMENTS

            var comments = BuildComments(users, books);
            _context.Comments.AddRange(comments);
            _context.SaveChanges();

            #endregion

            #region CREATE DESCRIPTIONS

            var descriptions = BuildDescriptions(books);
            _context.Descriptions.AddRange(descriptions);
            _context.SaveChanges();

            #endregion

            #region CREATE BOOK AUTHORS

            var bookAuthors = BuildBookAuthors(books, authors);
            _context.BookAuthors.AddRange(bookAuthors);
            _context.SaveChanges();

            #endregion

            #region CREATE BOOK CATEGORIES

            var bookCategories = BuildBookCategories(categories, books);
            _context.BookCategories.AddRange(bookCategories);
            _context.SaveChanges();

            #endregion

            #region CREATE BOOK RATINGS

            var bookRatings = BuildBookRatings(books);
            _context.BookRatings.AddRange(bookRatings);
            _context.SaveChanges();

            #endregion
        }

        private List<Author> BuildAuthors()
        {
            var authors = new List<Author>
            {
                new Author() {AuthorName = "Adam", AuthorSurname = "Mickiewicz"},
                new Author() {AuthorName = "Henryk", AuthorSurname = "Sienkiewicz"},
                new Author() {AuthorName = "Eliza", AuthorSurname = "Orzeszkowa"}
            };


            return authors;
        }

        private List<User> BuildUsers()
        {
            var users = new List<User>
            {
                new User() {UserName = "Janek", Email = "janek@o2.pl", IsActiveUser = true, IsBannedUser = false},
                new User()
                {
                    UserName = "Olgierd", Email = "olgierd@o2.pl", IsActiveUser = true, IsBannedUser = true
                },
                new User() {UserName = "Michał", Email = "michał@o2.pl", IsActiveUser = false, IsBannedUser = false}
            };


            return users;
        }

        private List<Category> BuildCategories()
        {
            var categories = new List<Category>
            {
                new Category() {Name = "Drama"},
                new Category() {Name = "Fantastyka"},
                new Category() {Name = "Komedia"}
            };


            return categories;
        }

        private List<Photo> BuildPhotos(List<Book> books)
        {
            var photos = new List<Photo>
            {
                new Photo() {ImageHref = "photoImageHref", Book = books[0]},
                new Photo() {ImageHref = "photoImageHref2", Book = books[1]},
                new Photo() {ImageHref = "photoImageHref3", Book = books[1]}
            };

            return photos;
        }

        private List<Book> BuildBooks(List<User> users)
        {
            var books = new List<Book>()
            {
                new Book()
                {
                    Title = "Pan Tadeusz",
                    Pages = 600,
                    User = users[0]
                },

                new Book()
                {
                    Title = "Nad Niemnem",
                    Pages = 400,
                    User = users[1]
                },
                new Book()
                {
                    Title = "Ogniem i Mieczem",
                    Pages = 1000
                }
            };

            return books;
        }

        private List<Comment> BuildComments(List<User> users, List<Book> books)
        {
            var comments = new List<Comment>()
            {
                new Comment()
                {
                    Date = DateTime.Now.Date,
                    Contents = "Test contents",
                    User = users[0],
                    Book = books[0]
                },
                new Comment()
                {
                    Date = DateTime.Now.Date,
                    Contents = "Test contents 1",
                    User = users[1],
                    Book = books[1]
                }
            };

            var comment = new Comment()
            {
                Date = DateTime.Now.Date,
                Contents = "Test contents 2",
                User = users[2],
                Book = books[2],
                AnotherComment = comments[0]
            };
            
            comments.Add(comment);

            return comments;
        }

        private List<Description> BuildDescriptions(List<Book> books)
        {
            var descriptions = new List<Description>()
            {
                new Description()
                {
                    Contents = "Test contents",
                    Book = books[0]
                },
                new Description()
                {
                    Contents = "Test contents 1",
                    Book = books[1]
                },
                new Description()
                {
                    Contents = "Test contents 2",
                    Book = books[2]
                }
            };

            return descriptions;
        }

        private List<BookAuthor> BuildBookAuthors(List<Book> books, List<Author> authors)
        {
            var bookAuthors = new List<BookAuthor>()
            {
                new BookAuthor()
                {
                    Book = books[0],
                    Author = authors[0]
                },
                new BookAuthor()
                {
                    Book = books[0],
                    Author = authors[1]
                },
                new BookAuthor()
                {
                    Book = books[1],
                    Author = authors[1]
                },
                new BookAuthor()
                {
                    Book = books[2],
                    Author = authors[2]
                }
            };

            return bookAuthors;
        }

        private List<BookCategory> BuildBookCategories(List<Category> categories, List<Book> books)
        {
            var bookCategories = new List<BookCategory>()
            {
                new BookCategory()
                {
                    Book = books[0],
                    Category = categories[0]
                },
                new BookCategory()
                {
                    Book = books[0],
                    Category = categories[2]
                },
                new BookCategory()
                {
                    Book = books[1],
                    Category = categories[1]
                },
                new BookCategory()
                {
                    Book = books[2],
                    Category = categories[2]
                }
            };

            return bookCategories;
        }

        private List<BookRating> BuildBookRatings(List<Book> books)
        {
            var bookRatings = new List<BookRating>()
            {
                new BookRating()
                {
                    Value = 3,
                    Book = books[0]
                },
                new BookRating()
                {
                    Value = 5,
                    Book = books[0]
                },
                new BookRating()
                {
                    Value = 5,
                    Book = books[1]
                },
                new BookRating()
                {
                    Value = 4,
                    Book = books[2]
                }
            };

            return bookRatings;
        }
    }
}